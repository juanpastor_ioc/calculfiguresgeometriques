package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author juan
 */
public class Circulo implements FiguraGeometrica {
    
    private final double radio;

    public Circulo() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Introduce el radio del círculo: ");
            this.radio = scanner.nextDouble();
            if (radio <= 0) {
                throw new IllegalArgumentException("El radio del círculo debe ser positivo");
            }
        } catch (InputMismatchException e) {
            throw new IllegalArgumentException("Entrada inválida. Debes introducir un número.");
        } finally {
            scanner.close(); // Cerrar el scanner para evitar fugas de recursos
        }
    }

    @Override
    public double calcularArea() {
        return Math.PI * radio * radio;
    }

    @Override
    public double calcularPerimetre() {
        return 2 * Math.PI * radio;
    }
    
}
