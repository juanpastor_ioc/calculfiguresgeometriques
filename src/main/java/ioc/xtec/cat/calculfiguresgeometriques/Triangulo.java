
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author juan
 */
public class Triangulo implements FiguraGeometrica{
    
    private final double costat;
    private final double altura;
    
    public Triangulo() {
        try (Scanner scanner = new Scanner(System.in)) { // Cerrar el scanner para evitar fugas de recursos
        
            System.out.println("Introduce la longitud de la base del triángulo: ");
            this.costat = scanner.nextDouble();
            if (costat <= 0) {
                throw new IllegalArgumentException("La longitud de la base debe ser positiva");
            }
            System.out.println("Introduce la longitud de la altura del triángulo: ");
            this.altura = scanner.nextDouble();
            if (altura <= 0) {
                throw new IllegalArgumentException("La longitud de la altura debe ser positiva");
            }
        } catch (InputMismatchException e) {
            throw new IllegalArgumentException("Entrada inválida. Debes introducir un número.");
        }
    }

    @Override
    public double calcularArea() {
        return 0.5 * costat * altura;
    }

    @Override
    public double calcularPerimetre() {
        return 3 * costat;
    }
}
